export const InitWebSchemeHandler: () => number;
export const enableDebug: () => number;
export const SetResponseHeaderWithRequestMethod: (method: string, key: string, value: string) => number;